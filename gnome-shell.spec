%define eds_version 3.45.1
%define gnome_desktop_version 40
%define glib2_version 2.56.0
%define gobject_introspection_version 1.49.1
%define gjs_version 1.73.1
%define gtk3_version 3.15.0
%define gtk4_version 4.0.0
%define adwaita_version 1.0.0
%define mutter_version 44
%define polkit_version 0.100
%define gsettings_desktop_schemas_version 42
%define ibus_version 1.5.2
%define gnome_bluetooth_version 1:42.3
%define gstreamer_version 1.4.5
%define pipewire_version 0.3.0
%define gnome_settings_daemon_version 3.37.1

Name:           gnome-shell
Version:        44.6
Release:        1
Summary:        Core user interface functions for the GNOME 3 desktop
Group:          User Interface/Desktops
License:        GPLv2+
URL:            https://wiki.gnome.org/Projects/GnomeShell
Source0:        https://download.gnome.org/sources/gnome-shell/44/%{name}-%{version}.tar.xz
Patch10001:     gnome-shell-favourite-apps-firefox.patch
Patch40001:     0001-gdm-Work-around-failing-fingerprint-auth.patch

BuildRequires:  bash-completion gcc meson mesa-libGL-devel mesa-libEGL-devel 
BuildRequires:  gtk-doc desktop-file-utils python3 librsvg2-devel chrpath cmake
BuildRequires:  gettext >= 0.19.6 libXfixes-devel >= 5.0
BuildRequires:  gcr4-devel pkgconfig(gnome-autoar-0) pkgconfig(gnome-desktop-3.0) pkgconfig(libpulse)
BuildRequires:  pkgconfig(libnm) pkgconfig(libsystemd) pkgconfig(libstartup-notification-1.0)
BuildRequires:  pkgconfig(gcr-4)
BuildRequires:  pkgconfig(ibus-1.0) >= %{ibus_version}
BuildRequires:  pkgconfig(libedataserver-1.2) >= %{eds_version}
BuildRequires:  pkgconfig(gjs-1.0) >= %{gjs_version}
BuildRequires:  pkgconfig(gio-2.0) >= %{glib2_version}
BuildRequires:  pkgconfig(gobject-introspection-1.0) >= %{gobject_introspection_version}
BuildRequires:  pkgconfig(polkit-agent-1) >= %{polkit_version}
BuildRequires:  pkgconfig(gstreamer-base-1.0) >= %{gstreamer_version}
BuildRequires:  pkgconfig(libpipewire-0.3) >= %{pipewire_version}
BuildRequires:  pkgconfig(gdk-x11-3.0) >= %{gtk3_version}
BuildRequires:  pkgconfig(gtk4) >= %{gtk4_version}
BuildRequires:  mutter-devel >= %{mutter_version}
BuildRequires:  gnome-bluetooth-libs-devel >= %{gnome_bluetooth_version}

Requires:       xdg-user-dirs-gtk
Requires:       libgnomekbd gnome-control-center switcheroo-control
Requires:       gnome-bluetooth%{?_isa} >= %{gnome_bluetooth_version}
Requires:       gnome-desktop3%{?_isa} >= %{gnome_desktop_version}
Requires:       gcr4%{?_isa}
Requires:       gobject-introspection%{?_isa} >= %{gobject_introspection_version}
Requires:       gjs%{?_isa} >= %{gjs_version}
Requires:       gtk3%{?_isa} >= %{gtk3_version}
Requires:       gtk4%{?_isa} >= %{gtk4_version}
Requires:       libadwaita%{_isa} >= %{adwaita_version}
Requires:       libnma%{?_isa}
Requires:       highcontrast-icon-theme
Requires:       librsvg2%{?_isa}
Requires:       mutter%{?_isa} >= %{mutter_version}
Requires:       upower%{?_isa}
Requires:       polkit%{?_isa} >= %{polkit_version}
Requires:       gnome-desktop3%{?_isa} >= %{gnome_desktop_version}
Requires:       glib2%{?_isa} >= %{glib2_version}
Requires:       gsettings-desktop-schemas%{?_isa} >= %{gsettings_desktop_schemas_version}
Requires:       gnome-settings-daemon%{?_isa} >= %{gnome_settings_daemon_version}
Requires:       gstreamer1%{?_isa} >= %{gstreamer_version}
Requires:       gstreamer1-plugins-good%{?_isa}
Requires:       pipewire-gstreamer%{?_isa}
Requires:       at-spi2-atk%{?_isa}
Requires:       ibus%{?_isa} >= %{ibus_version}
Requires:       accountsservice-libs%{?_isa}
Requires:       gdm-libs%{?_isa}
Requires:       python3%{_isa}
Requires:       geoclue2-libs%{?_isa}
Requires:       libgweather%{?_isa} >= 4.2.0
Requires:       bolt%{?_isa}
Requires:       xdg-desktop-portal-gtk >= 1.8.0
# Disabled on RHEL 7 to allow logging into KDE session by default
Recommends:     gnome-session-xsession
# needed by the welcome dialog
Recommends:     gnome-tour

Provides:       desktop-notification-daemon = %{version}-%{release}
Provides:       PolicyKit-authentication-agent = %{version}-%{release}
Provides:       bundled(gvc)
Provides:       bundled(libcroco) = 0.6.13

Conflicts:      gnome-shell-extension-background-logo < 3.34.0

%description
The GNOME Shell redefines user interactions with the GNOME desktop. In particular,
it offers new paradigms for launching applications, accessing documents, and
organizing open windows in GNOME. Later, it will introduce a new applets eco-system
and offer new solutions for other desktop features, such as notifications and contacts
management. The GNOME Shell is intended to replace functions handled by the GNOME Panel
and by the window manager in previous versions of GNOME. The GNOME Shell has rich
visual effects enabled by new graphical technologies.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
%meson -Dextensions_app=false
%meson_build

%install
%meson_install

# Create empty directories where other packages can drop extensions
mkdir -p %{buildroot}%{_datadir}/gnome-shell/extensions
mkdir -p %{buildroot}%{_datadir}/gnome-shell/search-providers

%find_lang %{name}

chrpath -d %{buildroot}%{_bindir}/gnome-shell
chrpath -d %{buildroot}%{_libdir}/%{name}/lib*.so
mkdir -p %{buildroot}/etc/ld.so.conf.d
echo "%{_bindir}/%{name}" > %{buildroot}/etc/ld.so.conf.d/%{name}-%{_arch}.conf
echo "%{_libdir}/%{name}" >> %{buildroot}/etc/ld.so.conf.d/%{name}-%{_arch}.conf

%check
desktop-file-validate %{buildroot}%{_datadir}/applications/org.gnome.Shell.desktop
desktop-file-validate %{buildroot}%{_datadir}/applications/org.gnome.Shell.Extensions.desktop
desktop-file-validate %{buildroot}%{_datadir}/applications/org.gnome.Shell.PortalHelper.desktop

%preun
/sbin/ldconfig
glib-compile-schemas --allow-any-name %{_datadir}/glib-2.0/schemas &> /dev/null ||:

%posttrans
/sbin/ldconfig
glib-compile-schemas --allow-any-name %{_datadir}/glib-2.0/schemas &> /dev/null ||:

%files -f %{name}.lang
%license COPYING
%doc README.md
%{_bindir}/gnome-*
%{_datadir}/glib-2.0/schemas/*.xml
%{_datadir}/glib-2.0/schemas/00_org.gnome.shell.gschema.override
%{_datadir}/applications/org.gnome.Shell.Extensions.desktop
%{_datadir}/applications/org.gnome.Shell.desktop
%{_datadir}/applications/org.gnome.Shell.PortalHelper.desktop
%{_datadir}/bash-completion/completions/gnome-extensions
%{_datadir}/gnome-control-center/keybindings/50-gnome-shell-launchers.xml
%{_datadir}/gnome-control-center/keybindings/50-gnome-shell-screenshots.xml
%{_datadir}/gnome-control-center/keybindings/50-gnome-shell-system.xml
%{_datadir}/gnome-shell/

%{_datadir}/dbus-1/services/org.gnome.ScreenSaver.service
%{_datadir}/dbus-1/services/org.gnome.Shell.CalendarServer.service
%{_datadir}/dbus-1/services/org.gnome.Shell.Extensions.service
%{_datadir}/dbus-1/services/org.gnome.Shell.HotplugSniffer.service
%{_datadir}/dbus-1/services/org.gnome.Shell.Notifications.service
%{_datadir}/dbus-1/services/org.gnome.Shell.PortalHelper.service
%{_datadir}/dbus-1/services/org.gnome.Shell.Screencast.service
%{_datadir}/dbus-1/interfaces/org.gnome.Shell.Extensions.xml
%{_datadir}/dbus-1/interfaces/org.gnome.Shell.Introspect.xml
%{_datadir}/dbus-1/interfaces/org.gnome.Shell.PadOsd.xml
%{_datadir}/dbus-1/interfaces/org.gnome.Shell.Screencast.xml
%{_datadir}/dbus-1/interfaces/org.gnome.Shell.Screenshot.xml
%{_datadir}/dbus-1/interfaces/org.gnome.ShellSearchProvider.xml
%{_datadir}/dbus-1/interfaces/org.gnome.ShellSearchProvider2.xml
%{_datadir}/icons/hicolor/scalable/apps/org.gnome.Shell.Extensions.svg
%{_datadir}/icons/hicolor/symbolic/apps/org.gnome.Shell.Extensions-symbolic.svg

%{_userunitdir}/org.gnome.Shell-disable-extensions.service
%{_userunitdir}/org.gnome.Shell.target
%{_userunitdir}/org.gnome.Shell@wayland.service
%{_userunitdir}/org.gnome.Shell@x11.service

%dir %{_datadir}/xdg-desktop-portal/portals/
%{_datadir}/xdg-desktop-portal/portals/gnome-shell.portal
%{_libdir}/gnome-shell/
%{_libexecdir}/gnome-shell-calendar-server
%{_libexecdir}/gnome-shell-perf-helper
%{_libexecdir}/gnome-shell-hotplug-sniffer
%{_libexecdir}/gnome-shell-portal-helper
%config(noreplace) /etc/ld.so.conf.d/*

%files help
%{_mandir}/man1/gnome-extensions.1*
%{_mandir}/man1/gnome-shell.1*

%changelog
* Mon Nov 27 2023 lwg <liweiganga@uniontech.com> - 44.6-1
- update to version 44.6

* Mon Jan 2 2023 lin zhang <lin.zhang@turbolinux.com.cn> - 43.2-2
- Delete adaptor_gcr-3.patch and using default gcr4 to build this package.

* Mon Jan 2 2023 lin zhang <lin.zhang@turbolinux.com.cn> - 43.2-1
- Upgrade to 43.2

* Wed Jul 06 2022 chenchen <chen_aka_jan@163.com> - 42.2-2
- change gnome-session-xsession and gnome-tour to "Recommends"

* Mon Mar 28 2022 lin zhang <lin.zhang@turbolinux.com.cn> - 42.2-1
- Upgrade to 42.2

* Tue Sep 07 2021 chenchen <chen_aka_jan@163.com> - 3.38.4-4
- del rpath from some binaries and bin

* Fri Jul 30 2021 chenyanpanHW <chenyanpan@huawei.com> - 3.38.4-3
- DESC: delete -Sgit from %autosetup, and delete BuildRequires git

* Wed Jun 23 2021 weijin deng <weijin.deng@turbolinux.com.cn> - 3.38.4-2
- Delete requires gdm-libs which gdm contains it
- Use pipewire replace pipewire-gstreamer which pipewire contains it
- Add xdg-desktop-portal-gtk for launching flatpak apps etc

* Mon May 31 2021 weijin deng <weijin.deng@turbolinux.com.cn> - 3.38.4-1
- Upgrade to 3.38.4
- Update Version, Release, Source0, BuildRequires, Requires
- Delete patches which existed in current version, modify one patch
- Update stage 'build', 'check', 'files'

* Tue Mar 30 2021 wangyue<wangyue92@huawei.com> - 3.30.1-7
- fix CVE-2020-17489

* Thu Dec 03 2020 wangxiao<wangxia65@huawei.com> -3.30.1-6
- move the libcroco sources directly under src/st
  remove the libcroco dependency from the meson.build files

* Fri Dec 27 2019 Jiangping Hu<hujiangping@huawei.com> - 3.30.1-5
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:remove the xdg-desktop-portal-gtk in recommends

* Wed Nov 27 2019 openEuler Buildteam<buildteam@openeuler.org> - 3.30.1-4
- Package Init
